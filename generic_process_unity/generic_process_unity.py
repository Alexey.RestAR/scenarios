all = [scene.getRoot()]

algo.repairCAD(all, 0.100000, True)

# Medium quality preset
algo.tessellate(all, 0.200000, -1, -1, True, 0, 1, 0.000000, False, False, True, False)

# Repair instances
scenario.createInstancesBySimilarity(all, 0.980000, 0.980000, True, False)

# Deletes lines (not required)
algo.deleteLines(all)

# Stitch edges
algo.repairMesh(all, 0.100000, True, False)

# Delete duplicated parts
scene.selectDuplicated(0.010000, 0.100000, 0.010000, 0.100000)
scene.deleteSelection()

# Not mandatory but can help reducing polycount
algo.removeHoles(all, True, True, True, 10.000000, 0)

# Delete patches (minimizes the number of submeshes in Unity)
algo.deletePatches(all, True)

# Cleanup almost coplanar triangles
algo.decimate(all, 0.2, 1, 8)

# If the inside is not important
# Reduces the polycount by a lot
algo.hiddenRemoval(all, 2, 1024, 16, 90.000000, False)

# Reduces the draw calls (very important for lightweight devices)
# DISABLE THIS TO PRESERVE HIERARCHY
scene.mergeParts(all, 2)

# Create normals if missing (lazy)
algo.createNormals(all, -1.000000, False, True)

# Create UVs if missing (lazy)
algo.mapUvOnAABB(all, False, 100.000000, 0, False)
algo.copyUV(all, 0, 1)

# Create tangents (lazy)
algo.createTangents(all, -1, 1, False)

# Repack UVs and maximizes used UV space for the Unity lightmapper
algo.repackUV(all, 0, True, 1024, 2, False, 3, True)
algo.normalizeUV(all, 1)