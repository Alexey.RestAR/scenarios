# Generic Process Unity

A generic preparation and optimization process for realtime visualization in Unity.  
See the steps in the script itself.  
(This sample process covers -most- usecases, but not all. The preparation process is often contextual to the type of asset, the type of app created and the device that will run the app)