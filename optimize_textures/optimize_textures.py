MAX_TEXTURE_SIZE = [1024, 1024]


def optimize_textures():
	for image in material.getAllImages():
		if image > 0: resize(image, MAX_TEXTURE_SIZE)


def resize(image, max_size):
	definition = material.getImageDefinition(image)
	if definition.width < max_size[0] and definition.height < max_size[1]: return
	ratio_width  = max_size[0]/definition.width
	ratio_height = max_size[1]/definition.height
	
	if ratio_width >= 1 and ratio_height >= 1: return
	factor = min(ratio_width, ratio_height)
	material.resizeImage(image, int(factor * definition.width), int(factor * definition.height))
	
optimize_textures()