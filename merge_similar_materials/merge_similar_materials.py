import math

RATIO = 0.3

def rgb_distance(rgb1, rgb2):
	return math.sqrt((rgb2[0] - rgb1[0])*(rgb2[0] - rgb1[0]) + (rgb2[1] - rgb1[1])*(rgb2[1] - rgb1[1]) + (rgb2[2] - rgb1[2])*(rgb2[2] - rgb1[2]))/3

def mean(a):
  return sum(a) / len(a)

def rgb_mean(rgb_list):
	return list(map(mean, zip(*rgb_list)))

def get_material_rgb(mat):
	material_type = material.getMaterialPatternType(mat)
	if material_type == pxz.material.MaterialPatternType.COLOR:
		return eval(core.getProperty(mat, 'color'))[:-1]
	elif material_type == pxz.material.MaterialPatternType.STANDARD:
		diffuse = core.getProperty(mat, 'diffuse')
		if 'TEXTURE' not in diffuse: 
			return eval(diffuse.replace('COLOR', ''))
	elif material_type == pxz.material.MaterialPatternType.PBR:
		albedo = core.getProperty(mat, 'albedo')
		if 'TEXTURE' not in albedo:
			return eval(albedo.replace('COLOR', ''))
	return None

def set_material_rgb(mat, rgb):
	material_type = material.getMaterialPatternType(mat)
	if material_type == pxz.material.MaterialPatternType.COLOR:
		if len(rgb) == 3: rgb.append(1) # color property should contain alpha value
		core.setProperty(mat, 'color', str(rgb))
	elif material_type == pxz.material.MaterialPatternType.STANDARD:
		diffuse = core.getProperty(mat, 'diffuse')
		if 'TEXTURE' not in diffuse: 
			core.setProperty(mat, 'diffuse', 'COLOR(' + str(rgb) + ')')
	elif material_type == pxz.material.MaterialPatternType.PBR:
		albedo = core.getProperty(mat, 'albedo')
		if 'TEXTURE' not in albedo:
			core.setProperty(mat, 'albedo', 'COLOR(' + str(rgb) + ')')

def step_progression():
	if core.interruptionRequested():
		core.addConsoleVerbose(2)
		core.popProgression()
		return False
	core.stepProgression()
	return True

def merge_similar_materials(ratio):
	core.removeConsoleVerbose(2)
	scene_root = scene.getRoot()
	replaced_materials = list()
	materials = material.getAllMaterials()
		
	materials_dict = dict()
	for mat in materials:
		rgb = get_material_rgb(mat)
		if rgb != None:
			dist_to_0 = rgb_distance(rgb, [0, 0, 0]) 	
			materials_dict[mat] = [rgb, dist_to_0]
	
	sorted_maps = sorted(materials_dict.items(), key=lambda item: item[1][1])
	
	core.pushProgression(len(sorted_maps))
	
	for id_1, map in sorted_maps:
		if not step_progression(): return
		if materials_dict[id_1] == None: continue # already merged
		diffuse_1, dist = map
		group = [id_1]
		diffuses = [diffuse_1]
		for id_2, diffuse_2 in materials_dict.items():
			if id_1 == id_2: continue
			if diffuse_2 == None: continue # already merged
			if rgb_distance(diffuse_1, diffuse_2[0]) < ratio:
				group.append(id_2)
				diffuses.append(diffuse_2[0])
		
		new_rgb = rgb_mean(diffuses)
		set_material_rgb(id_1, new_rgb)
		materials_dict[id_1] = None
		for mat in group:
			if mat == id_1: continue
			scene.replaceMaterial(mat, id_1, [scene_root])
			replaced_materials.append(mat)
			materials_dict[mat] = None
		materials_dict[id_1] = None

	core.deleteEntities(replaced_materials)
	core.popProgression()
	core.addConsoleVerbose(2)
	
n_0 = len(material.getAllMaterials())
merge_similar_materials(RATIO)
n_1 = len(material.getAllMaterials())
print('Resulting number of materials : ' + str(n_1))
if n_0 != 0:
	print('Reduction of ' + str(round((n_0 - n_1)/n_0*100, 1)) + '%%')