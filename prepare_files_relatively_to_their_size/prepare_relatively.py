import math

def point_sub(pt1, pt2):
	return [point_x(pt1)-point_x(pt2), point_y(pt1)-point_y(pt2), point_z(pt1)-point_z(pt2)]

def point_x(pt):
		return pt[0]

def point_y(pt):
		return pt[1]

def point_z(pt):
		return pt[2]

def dot_product(pt1, pt2):
	return point_x(pt1)*point_x(pt2) + point_y(pt1)*point_y(pt2) + point_z(pt1)*point_z(pt2)

def vector_length(v):
  return math.sqrt(dot_product(v,v))

def get_size(occurrence):
		aabb = scene.getAABB([occurrence])
		return vector_length(point_sub(aabb[1], aabb[0]))

def prepare_relatively(root):
	"""
	For small bounding boxes (1m and less), adjust tolerance and sag parameters to adapt to the size of the model
	"""
	size = get_size(root)
	if size < 1000:
			base_size = size/10000
	else: base_size = 0.1
	algo.repairMesh([root], base_size, True, False)
	algo.repairCAD([root], base_size, False)
	algo.tessellate([root], base_size*2, -1, -1, True, 0, 1, 0.000000, False, False, False, False)

prepare_relatively(scene.getRoot())
