import math

def point_sub(pt1, pt2):
	return [point_x(pt1)-point_x(pt2), point_y(pt1)-point_y(pt2), point_z(pt1)-point_z(pt2)]

def point_x(pt):
		return pt[0]

def point_y(pt):
		return pt[1]

def point_z(pt):
		return pt[2]

def dot_product(pt1, pt2):
	return point_x(pt1)*point_x(pt2) + point_y(pt1)*point_y(pt2) + point_z(pt1)*point_z(pt2)

def vector_length(v):
  return math.sqrt(dot_product(v,v))

def get_size(occurrence):
		aabb = scene.getAABB([occurrence])
		return vector_length(point_sub(aabb[1], aabb[0]))

def delete_small_parts(root, ratio):
	"""
	Delete small parts (less than ratio of the global size of the model)
	"""
	size = get_size(root)
	scene.selectByMaximumSize(size * ratio, -1.000000)
	scene.deleteSelection()

delete_small_parts(scene.getRoot(), 0.1)