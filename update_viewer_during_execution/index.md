# Update Viewer During Execution

Runs a decimation in a thread, waits for 1 second for the viewer to refresh, and then start a new thread (and so on)  
The viewer updates on every iteration.