NORMAL 		= [0, 0, 0]
METALLIC 	= 0
ROUGHNESS = 0.35
AO 				= 1
OPACITY		= 1

core.removeConsoleVerbose(2)

materials = material.getAllMaterials()
core.pushProgression(len(materials))

for mat in materials:
	core.stepProgression()
	
	# check current pattern
	current_pattern = material.getMaterialPatternType(mat)
	
	# get albedo depending on material type
	albedo = ''
	opacity = OPACITY
	if current_pattern == pxz.material.MaterialPatternType.COLOR:
		albedo_list = core.getProperty(mat, 'color').strip('][').split(', ')
		albedo = 'COLOR([%s, %s, %s])' % (albedo_list[0], albedo_list[1], albedo_list[2])
		opacity = float(albedo_list[3])
	elif current_pattern == pxz.material.MaterialPatternType.STANDARD:
		albedo = core.getProperty(mat, 'diffuse')
	elif current_pattern == pxz.material.MaterialPatternType.UNLIT_TEXTURE:
		albedo = 'TEXTURE(' + core.getProperty(mat, 'unlittexture') + ')'
	elif current_pattern == pxz.material.MaterialPatternType.PBR: 
		# Already PBR => go to next material
		continue
	else:
		# Unknown type (custom) => go to next material
		continue
	
	# set type to PBR
	material.setMaterialPattern(mat, 'PBR')
	
	# set properties
	material.setPBRMaterialInfos(mat, [core.getProperty(mat, 'Name'),\
		['color', [0, 0, 0]],\
		['color', NORMAL],\
		['coeff', METALLIC],\
		['coeff', ROUGHNESS],\
		['coeff', AO],\
		['coeff', opacity]]\
	)

	core.setProperty(mat, 'albedo', albedo)

core.popProgression()
core.addConsoleVerbose(2)