# Look for a property given a name.
# If not found on a given occurrence, it will search for this property on each of its parents until it is found.
# Returns the value of the property if found, otherwise returns "None"
def get_metadata_recursive(occurrence, property):
	try:
		metadata = scene.getComponent(occurrence, 5)
		return scene.getMetadata(metadata, property)
	except:
		parent = scene.getParent(occurrence)
		if parent == scene.getRoot():
			return "None"
		else:
			return get_metadata_recursive(parent, property)

selectedOccurrences = scene.getSelectedOccurrences()
for occurrence in selectedOccurrences:
	print("-> : " + str(get_metadata_recursive(occurrence, "My Property Name")))