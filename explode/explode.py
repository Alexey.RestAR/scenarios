import ast


def get_aabb_center(occurrence):
	aabb = scene.getAABB([occurrence])
	center = [(x + y)/2 for x, y in zip(aabb[0], aabb[1])]
	return center


def get_position(occurrence):
	transform = ast.literal_eval(core.getProperty(occurrence, 'Transform'))
	position  = [x[-1] for x in transform[:3]]
	return position
	

def main():
	core.removeConsoleVerbose(2)
	if core.interruptionRequested(): return
	root_center = get_aabb_center(scene.getRoot())
	parts = scene.getPartOccurrences(scene.getRoot())
	core.pushProgression(len(parts))
	for part in parts:
		core.stepProgression()
		part_center = get_aabb_center(part)
		position = get_position(part)
		direction = [x - y for x, y in zip(part_center, root_center)]
		
		print(position)
		print(direction)
		
		matrix = [[1, 0, 0, position[0] + direction[0]], [0, 1, 0, position[1] + direction[1]], [0, 0, 1, position[2] + direction[2]], [0, 0, 0, 1]]
							
		scene.applyTransformation(part, matrix)
	core.popProgression()
	core.addConsoleVerbose(2)
		


main()